# ci-scripts

This is an example repository made for posts from GitLab CI series:

* [Keeping common scripts in GitLab CI](https://threedots.tech/post/keeping-common-scripts-in-gitlab-ci/)
* [Automatic Semantic Versioning in GitLab CI](https://threedots.tech/post/automatic-semantic-versioning-in-gitlab-ci/)

This repository was forked from https://gitlab.com/OduSamuel/ci-scripts.git
 - Branch of initial fork (https://gitlab.com/OduSamuel/ci-scripts/-/tree/e48b5d5cdeeb344e4f145a81d84081567a76c75f)
